import aiohttp
import asyncio
import json

"""API docs: https://developers.arcgis.com/rest/services-reference/enterprise/map-service.htm"""


query_url = 'https://maps.ottawa.ca/arcgis/rest/services/Address_Information/MapServer/0/query'


async def get_data(session, oids):
    oids_param = ', '.join([str(x) for x in oids])
    async with session.post(query_url,
                            params={'f': 'json'},
                            data={'outFields': '*', 'objectIds': oids_param}) as resp:
        data = await resp.json()
        return data


def get_field(d, f):
    if not all([x[f] == d[0][f] for x in d]):
        print(f'Warning: Field "{f}" does not match for all query results, ' +
              'returning first value')
    return d[0]


async def main():
    async with aiohttp.ClientSession() as session:
        # Need to set `where` to `1=1` to get all rows in the dataset
        async with session.get(query_url,
                               params={
                                   'outFields': '*',
                                   'where': '1=1',
                                   'f': 'json',
                                   'returnIdsOnly': 'true'}) as response:
            body = await response.text()
            objectIdDict = json.loads(body)
            objectIdChunks = [objectIdDict['objectIds'][i:i+1000]
                              for i in range(0, len(objectIdDict['objectIds']), 1000)]
            tasks = []
            for oids in objectIdChunks:
                tasks.append(asyncio.ensure_future(get_data(session, oids)))

            all_data = await asyncio.gather(*tasks)
            all_data_collated = {
                'displayFieldName': get_field(all_data, 'displayFieldName'),
                'fieldAliases': get_field(all_data, 'fieldAliases'),
                'geometryType': get_field(all_data, 'geometryType'),
                'spatialReference': get_field(all_data, 'spatialReference'),
                'fields': get_field(all_data, 'fields'),
                'features': [x for sublist in all_data for x in sublist['features']],
            }

            with open('data/municipal.json', 'w') as f:
                json.dump(all_data_collated, f)


def download():
    """
    Downloads municipal address points from Ottawa's arcgis API and saves it to
    `data/municipal.json`.
    """
    asyncio.run(main())
