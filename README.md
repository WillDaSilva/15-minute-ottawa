## Goal

This, but for Ottawa: https://nathenry.com/writing/2023-02-07-seattle-walkability.html

## To download required data (WIP)

```
python src/download/municipal_address_points
```

## TODO
- Find sources for all the other required data
- Add download scripts for all the other data sources
- Figure out project organization
- Convert all downloaded data to a common format

## Resources

### Interactive Maps in Python

https://plotly.com/python/maps/
